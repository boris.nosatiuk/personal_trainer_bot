Еще одно эффективное упражнение для мужчин и женщин, выполняемое с помощью тренажера. Обеспечивает грудным мышцам хорошую растяжку и придает рельефность.

Для начала расположитесь в тренажере. Выровняйте спину, следите, чтобы она была плотно прижата к спинке скамьи. Теперь возьмитесь за рукояти рычагов, слегка согните локтевые суставы. 
Предплечья при этом параллельны поверхности пола.

Ноги расставьте шире плеч, смотрите строго вперед. Выдыхая медленно сведите руки вместе, постарайтесь задержаться в таком положении на несколько секунд. На вдохе вернитесь в исходную позицию. 
Ощутите, как растягиваются мышцы груди.

Обратите внимание! Рукояти тренажера должны быть отрегулированы так, чтобы даже при максимальном разведении рук мышцы все равно оставались в напряжении. 
При выполнении груз всегда должен оставаться на весу, не касаясь опоры.


Выполните 3–5 подходов по 10–12 раз.
