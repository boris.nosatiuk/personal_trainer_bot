from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup


def group_of_muscles_inline():
    inline_btn_1 = InlineKeyboardButton('Грудные мышцы', callback_data='chest 1')
    inline_btn_2 = InlineKeyboardButton('Мышцы спины', callback_data='back 1')
    inline_btn_6 = InlineKeyboardButton('Трицепс', callback_data='triceps 1')
    inline_btn_7 = InlineKeyboardButton('Бицепс', callback_data='biceps 1')
    inline_btn_9 = InlineKeyboardButton('Тренировка для девушек', callback_data='girl 1')

    inline_kb_full = InlineKeyboardMarkup(row_width=2)
    inline_kb_full.add(inline_btn_1, inline_btn_2, inline_btn_6, inline_btn_7, inline_btn_9)
    return inline_kb_full


def next_exersice_markup(exersice, exersice_number):
    inline_btn_1 = InlineKeyboardButton('Поехали дальше!',
                                        callback_data=f'{exersice} {exersice_number + 1}')
    inline_btn_2 = InlineKeyboardButton('Заменить упражнение!',
                                        callback_data=f'{exersice} {exersice_number}')

    inline_kb_full = InlineKeyboardMarkup()
    inline_kb_full.add(inline_btn_1)
    inline_kb_full.row(inline_btn_2)
    return inline_kb_full
