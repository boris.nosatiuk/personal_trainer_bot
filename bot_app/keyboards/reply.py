from aiogram.types import KeyboardButton, ReplyKeyboardMarkup


def start_button():
    full_reply_button = ReplyKeyboardMarkup(resize_keyboard=True)
    button1 = KeyboardButton('Начать тренировку')
    button2 = KeyboardButton('Получить мотивацию!')
    full_reply_button.add(button1, button2)
    return full_reply_button
