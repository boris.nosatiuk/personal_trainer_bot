import os
import random

from aiogram import types

from bot_app.config import motivation_dir
from bot_app.keyboards import reply, inline
from bot_app.misc import dp, bot


@dp.message_handler(commands='start')
async def get_massage(message: types.Message):
    await bot.send_message(message.chat.id, text=f'Здравствуй, {message.from_user.username}, начнем тренировку!',
                           reply_markup=reply.start_button())


@dp.message_handler(text='Начать тренировку')
async def start_gym(message: types.Message):
    await bot.send_message(message.from_user.id, text='Выберите группу мышц, которую будем сегодня тренировать',
                           reply_markup=inline.group_of_muscles_inline())


@dp.message_handler(text='Получить мотивацию!')
async def motivation(message: types.Message):
    motivation_file = os.path.join(motivation_dir, 'motivation_list.txt')
    open_file = open(motivation_file, mode='r', encoding='utf-8').read()
    random_motivation = random.choice(open_file.split('\n'))
    await bot.send_message(message.chat.id, text='<b>' + random_motivation + '</b>')


@dp.message_handler()
async def echo_message(massage: types.Message):
    await bot.send_message(massage.from_user.id, text='Ошибочный ввод')

