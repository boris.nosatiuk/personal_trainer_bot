import os
import random

from aiogram.types import CallbackQuery

from bot_app.config import path_to_exercise_dir
from bot_app.misc import dp, bot
from bot_app.keyboards import inline


@dp.callback_query_handler(text='back 1')
async def get_chest_callback(call: CallbackQuery):
    await call.answer('Первое упражнение')

    dir_exercise = os.path.join(path_to_exercise_dir, 'back', 'first_exercise')
    list_dir = os.listdir(dir_exercise)
    random_dir = random.choice(list_dir)
    path_gif_file = os.path.join(dir_exercise, random_dir, '1.gif')
    path_txt_file = os.path.join(dir_exercise, random_dir, '1.txt')
    path_name_file = os.path.join(dir_exercise, random_dir, 'name_exercise.txt')

    await bot.send_message(call.from_user.id,
                           text=open(path_name_file, mode='r', encoding='utf-8').read())

    await bot.send_animation(call.from_user.id,
                             animation=open(path_gif_file, 'rb'))

    await bot.send_message(call.from_user.id,
                           text=open(path_txt_file, mode='r', encoding='utf-8').read(),
                           reply_markup=inline.next_exersice_markup(call.data.split(' ')[0],
                                                                    int(call.data.split(' ')[-1])))


@dp.callback_query_handler(text='back 2')
async def get_chest_callback(call: CallbackQuery):
    await call.answer('Второе упражнение')

    dir_exercise = os.path.join(path_to_exercise_dir, 'back', 'second_exercise')
    list_dir = os.listdir(dir_exercise)
    random_dir = random.choice(list_dir)
    path_gif_file = os.path.join(dir_exercise, random_dir, '1.gif')
    path_txt_file = os.path.join(dir_exercise, random_dir, '1.txt')
    path_name_file = os.path.join(dir_exercise, random_dir, 'name_exercise.txt')

    await bot.send_message(call.from_user.id,
                           text=open(path_name_file, mode='r', encoding='utf-8').read())

    await bot.send_animation(call.from_user.id,
                             animation=open(path_gif_file, 'rb'))

    await bot.send_message(call.from_user.id,
                           text=open(path_txt_file, mode='r', encoding='utf-8').read(),
                           reply_markup=inline.next_exersice_markup(call.data.split(' ')[0],
                                                                    int(call.data.split(' ')[-1])))


@dp.callback_query_handler(text='back 3')
async def get_chest_callback(call: CallbackQuery):
    await call.answer('Третье упражнение')

    dir_exercise = os.path.join(path_to_exercise_dir, 'back', 'third_exercise')
    list_dir = os.listdir(dir_exercise)
    random_dir = random.choice(list_dir)
    path_gif_file = os.path.join(dir_exercise, random_dir, '1.gif')
    path_txt_file = os.path.join(dir_exercise, random_dir, '1.txt')
    path_name_file = os.path.join(dir_exercise, random_dir, 'name_exercise.txt')

    await bot.send_message(call.from_user.id,
                           text=open(path_name_file, mode='r', encoding='utf-8').read())

    await bot.send_animation(call.from_user.id,
                             animation=open(path_gif_file, 'rb'))

    await bot.send_message(call.from_user.id,
                           text=open(path_txt_file, mode='r', encoding='utf-8').read(),
                           reply_markup=inline.next_exersice_markup(call.data.split(' ')[0],
                                                                    int(call.data.split(' ')[-1])))


@dp.callback_query_handler(text='back 4')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()

    await bot.send_message(call.from_user.id,
                           text='Вы закончили упражнения на данную группу мышц!',
                           reply_markup=inline.group_of_muscles_inline())
