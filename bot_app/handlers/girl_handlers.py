import os
import random

from aiogram.types import CallbackQuery

from bot_app.config import path_to_exercise_dir
from bot_app.misc import dp, bot
from bot_app.keyboards import inline


@dp.callback_query_handler(text='girl 1')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()

    dir_exercise = os.path.join(path_to_exercise_dir, 'for girl', 'main exercise', 'first_exercise')
    list_dir = os.listdir(dir_exercise)
    random_dir = random.choice(list_dir)
    path_gif_file = os.path.join(dir_exercise, random_dir, '1.gif')
    path_txt_file = os.path.join(dir_exercise, random_dir, '1.txt')
    path_name_file = os.path.join(dir_exercise, random_dir, 'name.txt')

    await bot.send_message(call.from_user.id,
                           text=open(path_name_file, mode='r', encoding='utf-8').read())

    await bot.send_animation(call.from_user.id,
                             animation=open(path_gif_file, 'rb'))

    await bot.send_message(call.from_user.id,
                           text=open(path_txt_file, mode='r', encoding='utf-8').read(),
                           reply_markup=inline.next_exersice_markup(call.data.split(' ')[0],
                                                                    int(call.data.split(' ')[-1])))


@dp.callback_query_handler(text='girl 2')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()

    dir_exercise = os.path.join(path_to_exercise_dir, 'for girl', 'main exercise', 'second_exercise')
    list_dir = os.listdir(dir_exercise)
    random_dir = random.choice(list_dir)
    path_gif_file = os.path.join(dir_exercise, random_dir, '1.gif')
    path_txt_file = os.path.join(dir_exercise, random_dir, '1.txt')
    path_name_file = os.path.join(dir_exercise, random_dir, 'name.txt')

    await bot.send_message(call.from_user.id,
                           text=open(path_name_file, mode='r', encoding='utf-8').read())

    await bot.send_animation(call.from_user.id,
                             animation=open(path_gif_file, 'rb'))

    await bot.send_message(call.from_user.id,
                           text=open(path_txt_file, mode='r', encoding='utf-8').read(),
                           reply_markup=inline.next_exersice_markup(call.data.split(' ')[0],
                                                                    int(call.data.split(' ')[-1])))


@dp.callback_query_handler(text='girl 3')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()

    dir_exercise = os.path.join(path_to_exercise_dir, 'for girl', 'main exercise', 'cardio')
    path_txt_file = os.path.join(dir_exercise, '1.txt')
    path_gif_file = os.path.join(dir_exercise, '1.gif')
    path_name_file = os.path.join(dir_exercise, 'name.txt')

    await bot.send_message(call.from_user.id,
                           text=open(path_name_file, mode='r', encoding='utf-8').read())

    await bot.send_message(call.from_user.id,
                           text=open(path_txt_file, mode='r', encoding='utf-8').read(),
                           reply_markup=inline.next_exersice_markup(call.data.split(' ')[0],
                                                                    int(call.data.split(' ')[-1])))


@dp.callback_query_handler(text='girl 4')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()

    await bot.send_message(call.from_user.id,
                           text='Красавчик, можешь идти отдыхать',
                           reply_markup=inline.group_of_muscles_inline())
